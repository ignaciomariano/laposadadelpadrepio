<?php include 'header.php'; ?>

        <div class="pagina">
            <div class="marco-secciones"><br/>
                <div class="titulo">TERAPIAS ORIENTALES INTEGRALES</div><br/>
                <span class="titulo">AROMATERAPIA</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/6.jpg" alt="6.jpg" width="180" height="180"/>
                    <img src="images/img/spa/7.jpg" alt="7.jpg" width="150" height="180"/>
                    <img src="images/img/spa/8.jpg" alt="8.jpg" width="150" height="180"/>
                    <img src="images/img/spa/9.jpg" alt="9.jpg" width="180" height="180"/>
                </div><br/>
                El termino Aromaterapia se invento gracias a un investigador franc&eacute;s, R-M Gattefos&eacute; (1881-
                1950) quien al quemarse la mano luego de sufrir una explosi&oacute;n en su laboratorio, tuvo el reflejo
                de sumergirla en un recipiente de aceite esencial de Lavanda (lav&aacute;ndula Angustifolia). Adem&aacute;s
                de sentir un alivio inmediato, la curaci&oacute;n y la cicatrizaci&oacute;n de la herida fue tan r&aacute;pida que
                Gattefos&eacute; se dedic&oacute; en adelante al estudio antibacteriano de los aceites esenciales.<br/><br/>
                <span class="titulo">CRISTALOTERAPIA / GEMOTERAPIA</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/10.jpg" alt="10.jpg" width="180" height="180"/>
                    <img src="images/img/spa/11.jpg" alt="11.jpg" width="150" height="180"/><br/><br/>
                </div>
                La Cristaloterapia o Gemoterapia Forma parte de los tratamientos vibracionales m&aacute;s eficaces
                para nivelar el flujo de nuestras energ&iacute;as, poni&eacute;ndonos en contacto directo con el subconsciente.
                La sanaci&oacute;n con cristales trabaja directamente con la luz, color, belleza y formas geom&eacute;tricas
                perfectas producidas en el reino mineral.<br/><br/>
                <span class="titulo">FLORES DE BACH</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/12.jpg" alt="12.jpg" width="240" height="180"/>
                    <img src="images/img/spa/13.jpg" alt="13.jpg" width="180" height="180"/>
                    <img src="images/img/spa/14.jpg" alt="14.jpg" width="220" height="180"/>
                </div><br/><br/>
                Flores de Bach, tambi&eacute;n llamadas remedios florales de Bach o esencias florales de Bach,
                es la denominaci&oacute;n gen&eacute;rica y comercial de un conjunto de 38 preparados naturales no
                farmacol&oacute;gicos, s&iacute;mil-homeop&aacute;ticos, elaborados a partir de una decocci&oacute;n o maceraci&oacute;n en agua
                de flores maduras de diversas especies vegetales silvestres o naturalizadas de la regi&oacute;n de Gales
                y la Inglaterra contigua, diluida en brandy (destilado del vino, usado como medio conservante.<br/><br/>
                <span class="titulo">MASOTERAPIA</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/18.jpg" alt="18.jpg" width="275" height="185"/>
                    <img src="images/img/spa/19.jpg" alt="19.jpg" width="275" height="185"/>
                </div><br/><br/>
                T&eacute;cnicamente, es un sistema que utiliza el Test Muscular como mecanismo de Bio-informaci&oacute;n,
                buscando en este estudio corregir los posibles desequilibrios que afectan a la persona. Son
                masajes variados, con piedras calientes con bolsas Pinda, o simplemente manuales.En un
                sentido m&aacute;s completo la masoterapiaes una forma de Comunicaci&oacute;n y estudio de todos los
                niveles que "conforman" al ser humano: F&iacute;sico, Qu&iacute;mico, Electromagn&eacute;tico, Emocional y
                Factor-X, tanto para saber comprender lo que est&aacute; pasando, como para conocer de qu&eacute; forma
                se pueden solucionar los diferentes problemas que padecemos. Un Masoterapeuta no hace
                diagn&oacute;sticos m&eacute;dicos, sino busca la causa del desequilibrio para que al solucionarse, se resuelva
                el s&iacute;ntoma. Es un Modelo Terap&eacute;utico que no lucha contra el s&iacute;ntoma, sino que refuerza y
                equilibra al sistema. El Masoterapeuta ayuda a su consultante a tomar conciencia del problema y
                su proceso, as&iacute; como de la explicaci&oacute;n de por qu&eacute; ha llegado a ese punto. Adem&aacute;s, le comunica
                desde su conocimiento m&aacute;s profundo para que el consultante decida lo que considera mejor para
                sanar su vida.<br/><br/>
                <span class="titulo">MASAJES</span><br/><br/>
                En la actualidad se coincide en definir al masaje como "una combinaci&oacute;n de movimientos
                t&eacute;cnicos manuales o maniobras realizadas armoniosa y met&oacute;dicamente, con fines higi&eacute;nico-
                preventivos y/o terap&eacute;uticos, que al ser aplicado con las manos permite valorar el estado de los
                tejidos tratados"; se emplea en medicina, kinesiolog&iacute;a, est&eacute;tica, deporte, etc.<br/><br/>
                <span class="titulo">MASAJES CON PIEDRAS CALIENTES</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/23.jpg" alt="23.jpg" width="325" height="155"/>
                    <img src="images/img/spa/24.jpg" alt="24.jpg" width="192" height="155"/>
                </div><br/><br/>
                El masaje con piedras calientes y fr&iacute;as no es nuevo. Desde hace muchos siglos, tanto la
                cultura China, como la de los indios americanos, utilizaban las piedras calientes para
                dar masaje y aliviar dolencias. Desde los puntos chakra, la energ&iacute;a es distribuida a todo
                el organismo, gracias a los numerosos canales energ&eacute;ticos que lo atraviesan y que est&aacute;n
                unidos a ellos. Concentrarse sobre los chakra ayuda, por lo tanto, a regular el flujo
                energ&eacute;tico en el cuerpo (demasiada energ&iacute;a en c&iacute;rculo o demasiado poca puede crear
                trastornos) . Precisamente, con esta finalidad, durante la "stone teraphy". las piedras
                calientes se disponen sobre los chakra y, a continuaci&oacute;n, se procede a la "apertura" y
                el "cierre" energ&eacute;ticos y a la conexi&oacute;n, tambi&eacute;n energ&eacute;tica, de los miembros superiores
                e inferiores.El elemento fundamental de la terapia geotermal, lo constituye la aplicaci&oacute;n
                de piedras calientes y fr&iacute;as que act&uacute;an a dos niveles:
                A) De forma est&aacute;tica, ejerciendo presiones terap&eacute;uticas en puntos concretos
                B) De forma din&aacute;mica realizando maniobras de masaje bien definidas
                "Las piedras calientes (50°) incrementan el riego sangu&iacute;neo y el metabolismo celular,
                mientras que las fr&iacute;as ( 8°) originan vasoconstricci&oacute;n y liberaci&oacute;n de histamina que
                act&uacute;a sobre el dolor y los procesos inflamatorios".<br/><br/>
                <span class="titulo">MASAJES CON PINDAS</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/25.jpg" alt="25.jpg" width="180" height="180"/>
                    <img src="images/img/spa/26.jpg" alt="26.jpg" width="180" height="180"/>
                    <img src="images/img/spa/27.jpg" alt="27.jpg" width="180" height="180"/>
                    <img src="images/img/spa/28.jpg" alt="28.jpg" width="180" height="180"/>
                </div><br/><br/>
                El masaje con pindas es un tratamiento ayurv&eacute;dico inspirado en el masaje thai cuya finalidad
                es el bienestar y relajaci&oacute;n del cuerpo y la mente. Sus maniobras proporcionan beneficios que
                depender&aacute;n de las plantas que se utilicen, y es parte de una forma de vida que poco a poco se
                ha ido introduciendo en Am&eacute;rica y que ha permitido a los terapeutas trabajar de una forma m&aacute;s
                hol&iacute;stica.<br/><br/>
                <span class="titulo">MEDITACIONES ACTIVAS</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/29.jpg" alt="25.jpg" width="180" height="180"/>
                    <img src="images/img/spa/30.jpg" alt="26.jpg" width="230" height="180"/>
                    <img src="images/img/spa/31.jpg" alt="27.jpg" width="260" height="180"/>
                </div><br/><br/>
                Hasta ahora se pensaba que las t&eacute;cnicas de meditaci&oacute;n era dif&iacute;ciles, aburridas, poco asequibles
                y casi un patrimonio de los m&iacute;sticos. Sin embargo con las nuevas y modernas Meditaciones
                Activas, el hecho de meditar resulta una experiencia f&aacute;cil, sencilla y hasta divertida.
                Especialmente adaptadas a nuestra cultura occidental, todos podemos no s&oacute;lo practicarlas y
                gozar de sus beneficios, sino que ahora incluso dirigirlas y ayudar a los dem&aacute;s a encontrarse a s&iacute;
                mismos para ser m&aacute;s conscientes y felices. No es necesario adoptar la posici&oacute;n tradicional. Se
                puede realizar simplemente sentado en una silla.<br/><br/>
                <span class="titulo">REFLEXOLOGIA</span><br/><br/>    
                Reflexología o terapia zonal es la práctica de estimular puntos sobre los pies, manos, u orejas
                (llamados zonas de reflejo), con la esperanza de que tendrá un efecto benéfico sobre otras
                partes del cuerpo, o de que mejorará la salud general. La Reflexología trabaja el Sistema
                Nervioso a través de los dermatomas. Nuestro cuerpo esta provisto de terminaciones nerviosas 
                en el toda la superficie, siendo los pies y las manos los que ocupan una área más amplia respecto 
                a las demás partes del cuerpo.<br/><br/>
                <span class="titulo">REFLEXOLOGIA FACIAL</span><br/><br/>    
                <div class="img-center">
                    <img src="images/img/spa/35.jpg" alt="35.jpg" width="180" height="180"/>
                    <img src="images/img/spa/36.jpg" alt="36.jpg" width="270" height="180"/>
                    <img src="images/img/spa/37.jpg" alt="37.jpg" width="270" height="180"/><br/><br/>
                </div>
                Sin necesidad de las palabras, el rostro puede llegar a transmitir los sentimientos más secretos,
                incluso los que desconocemos. Nuestro rostro comprende más de 500 puntos interconectados con todo 
                el cuerpo. La reflexología facial, terapia introducida por Lone Sorensen, permite tratar estos puntos,
                actuando sobre la totalidad del sistema nervioso. Igualmente, cuando se presentan alteraciones de 
                la salud, el semblante comienza a transformarse silenciosamente, evidenciando los trastornos que
                experimenta el organismo, por lo que para muchos la cara puede llegar a representar la totalidad 
                corporal, tanto física como emocionalmente, siendo además la única zona del cuerpo donde se encuentran 
                todos los órganos de los sentidos.<br/><br/>
                <span class="titulo">REFLEXOLOGIA PODAL</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/38.jpg" alt="38.jpg" width="140" height="180"/>
                    <img src="images/img/spa/39.jpg" alt="39.jpg" width="300" height="190"/>
                    <img src="images/img/spa/40.jpg" alt="40.jpg" width="300" height="190"/>
                </div><br/><br/>
                En la actualidad, prestamos muy poca importancia a los pies, es el gran olvidado de
                nuestro cuerpo y sin embargo, es la parte del cuerpo donde van a parar multitud de
                terminaciones nerviosas.
                La planta del pie y el pie en su conjunto, representa en forma esquem&aacute;tica la totalidad
                del cuerpo y sus &oacute;rganos. Este es el principio de la reflexolog&iacute;a podal.
                En ella, mediante el masaje se puede influir a modo reflejo sobre los correspondientes
                &oacute;rganos y v&iacute;sceras, aparte de la importancia diagn&oacute;stica que tiene.
                Los buenos resultados de la reflexolog&iacute;a podal y la escasez de efectos secundarios, hace
                que hoy en d&iacute;a, sea aplicada con m&aacute;s frecuencia por los profesionales sanitarios, entre
                ellos los fisioterapeutas.
                La idea de que manipulando los pies con las manos y las yemas de los dedos se pueden
                estimular las diferentes zonas del organismo, facilitando la curaci&oacute;n y previniendo la
                enfermedad, ya formaba parte de muchas terapias m&eacute;dicas de la antigüedad, desde el Egipto
                cl&aacute;sico a la Antigua India.<br/><br/>
                el reikista viendo la situaci&oacute;n del paciente.<br/><br/>
                <span class="titulo">RELAJACI&Oacute;N</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/47.jpg" alt="47.jpg" width="282" height="175"/>
                    <img src="images/img/spa/48.jpg" alt="48.jpg" width="295" height="175"/>
                    <img src="images/img/spa/49.jpg" alt="49.jpg" width="200" height="175"/>
                </div><br/><br/>
                El estilo de vida actual, y sobretodo, en aglomeraciones urbanas, hay una serie de situaciones
                añadidas, que pueden convertirse en agentes estresantes como: el ruido ambiental, el tr&aacute;fico
                lento, la alta competitividad, la conciliaci&oacute;n de la vida laboral y profesional… Todo ello nos
                puede llevar a situaciones de estr&eacute;s. Cada vez se hace m&aacute;s imprescindible pues, la pr&aacute;ctica
                diaria de alguna t&eacute;cnica de relajaci&oacute;n, hasta convertirla en un h&aacute;bito, en parte de nuestra rutina
                diaria.<br/><br/>
                <span class="titulo">RESUMEN DE NUESTROS SERVICIOS DE SPA:</span><br/><br/>
                <div style="text-align: center">
                    *Masaje Relajante y Descontracturante mixto manual y con Piedras.<br/>
                    *Aromaterapia.<br/>
                    *Reflexolog&iacute;a Integral.<br/>
                    *Masaje con piedras calientes.<br/>
                    *Masaje Hind&uacute; Sweda con Pindas (Ayurveda).<br/>
                    *Masaje Oriental Integral.<br/>
                    *Masaje Facial Energizante.<br/>
                    *Masajes Descontracturantes y de relajaci&oacute;n.<br/>
                    *Gemoterapia con Piedras Semipreciosas y Elixir.<br/>
                    *Flores de Bach con Medicamentos y Energizaci&oacute;n.<br/>
                    *Masaje Reflexol&oacute;gico Podal manual.<br/>
                    *Gestión de Estrés.<br/><br/><br/>
                </div>
            </div>
        </div>


<?php include 'footer.php'; ?>