<?php include 'header.php'; ?>

    <div class="pagina">
        <div class="marco-secciones"><br/>
            <div class="titulo">Bienvenido al Hotel La Posada del Padre P&iacute;o</div><br/>
            <div class="img-index off">
                <img src="images/img/hotel/200/1.JPG" alt="1.JPG"/>
            </div>
            <div class="txt-index">
                Le brindamos una cordial bienvenida a nuestro hotel en Cosqu&iacute;n, Sierras de C&oacute;rdoba, 
                brind&aacute;ndole todas nuestras comodidades, ajustadas a las normas de seguridad y medio ambiente.<br/><br/>

                Nuestra filosof&iacute;a es tratar de brindar un servicio diferente donde la dignidad bio&eacute;tica del pasajero
                sea contemplada en todos sus aspectos y a precios totalmente razonables encuadrados dentro de lo que la tendencia hotelera internacional denominada “comunicacional”. Desde el desayuno buffet y los servicios
                de confiter&iacute;a - bar preparados por Chef diplomado, a cada una de las prestaciones junto la atenci&oacute;n esmerada del personal,
                ha sido din&aacute;micamente balanceada y estructurada en su favor.<br/><br/>
            </div>
            <div style="clear: both;"></div>
            Propendemos al descanso pleno y eficaz, por eso nuestras habitaciones no poseen televisor, siguiendo los &uacute;ltimos
            conceptos de hoteler&iacute;a de holganza, solaz, reposo y sosiego.<br/><br/>

        </div>
    </div>
            
<?php include 'footer.php'; ?>