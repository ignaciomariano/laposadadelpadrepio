<?php include 'header.php'; ?>

        <div class="pagina">
            <div class="marco-secciones"><br/>
                <span class="titulo" style="float: left;">Contacto</span>
                <br/><br/>
                <div style="text-align:center;">
                    Ante cualquier consulta escr&iacute;banos por medio de siguiente formulario o a trav&eacute;s de su 
                    programa de correo electr&oacute;nico favorito:
                    <a style="color:#0066FF" href='mailto:laposadadelpadrepio@hotmail.com.ar'>laposadadelpadrepio@hotmail.com.ar</a>
                </div><br/>
                <div id="container">
                        <div>
                            <label for="email">Email:</label>
                            <input id="email" type="text" size="30" onblur="comprobarEmail(this.value,'error-email')"/>
                            <span id="error-email"></span>
                        </div>
                        <div>
                            <label for="message">Mensaje:</label>
                            <textarea id="comentario" cols="30" rows="10"></textarea>
                        </div><br/>
                        <p class="submit"><button type="submit" onClick="subir_comentario()">Enviar</button></p>
                        <div id="box"></div>
                    <div style="clear: both;"></div>
                </div><br/><br/>
            </div>
        </div>

<?php include 'footer.php'; ?>