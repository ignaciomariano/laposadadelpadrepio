<?php include 'header.php'; ?>

        <div class="pagina">
            <div class="marco-secciones"><br/>
                <div class="titulo">TERAPIAS ORIENTALES INTEGRALES</div><br/>
                <div class="img-center">
                    <img src="images/img/spa/1.jpg" alt="1.jpg"/>
                </div>
                En los &uacute;ltimos años estamos viendo como una serie de tratamientos terap&eacute;uticos naturales est&aacute;n teniendo cada 
                vez mas demanda y aceptaci&oacute;n. Muchos de estas medicinas  mal llamadas alternativas por la medicina occidental,
                tienen su origen en Oriente y se vienen practicando desde hace miles de años, como es el caso del Ayurveda
                o la Medicina Tradicional China. Estas formas de medicina tienen como principal funci&oacute;n la prevenci&oacute;n de
                las enfermedades y el trabajar sobre la vitalidad y la salud de las personas.<br/><br/>
                <div>
                    <div style="width: 180px; float: left;">
                        <img src="images/img/spa/100.jpg" alt="100.jpg" height="200"/>
                    </div>
                    <div style="width: 600px; float: left;">
                        La filosof&iacute;a oriental siempre se ha interesado en descubrir cual es nuestro papel en el universo
                        y ha intentado entender las leyes del macrocosmos para poder
                        integrarnos mejor en &eacute;l y poder gozar de una vida que integre y
                        respete dichas leyes. Las terapias orientales intentan equilibrar la
                        vitalidad y la salud mediante la aplicaci&oacute;n de este conocimiento,
                        empleando su forma de entender el universo a la medicina. Por
                        esto es importante tener un conocimiento b&aacute;sico de su filosof&iacute;a para
                        poder entender mejor las diferentes formas de medicina oriental.
                    </div>
                </div>
                <div style="clear: both;"></div><br/>
                <div>
                    <div style="width: 280px; float: right;">
                        <img src="images/img/spa/2.jpg" alt="2.jpg"/>
                    </div>
                    <div style="width: 510px; float: left;">
                        Uno de las bases m&aacute;s importantes de la medicina
                        oriental es el trabajo sobre el KI (Qi en china o Prana en
                        la filosof&iacute;a hind&uacute;). Podr&iacute;amos definir el KI como la
                        fuerza vital que permite y sustenta toda expresi&oacute;n de
                        vida y que mantiene el equilibrio en el universo. De una manera muy global
                        podr&iacute;amos decir que el objetivo de los tratamientos orientales es el de tratar
                        de restablecer la circulaci&oacute;n de KI en el cuerpo, hacer que el KI estancado se
                        mueva, que el KI disperso se centre y que los canales por los que
                        circula est&eacute;n libres de obst&aacute;culos y el&aacute;sticos.
                    </div><br/><br/>
                </div><br/>
                <div style="clear: both;"></div>
                Dependiendo de las particularidades de cada civilizaci&oacute;n y regi&oacute;n se han desarrollado diferentes
                m&eacute;todos que persiguen este objetivo de una manera distinta.<br/><br/>

                <div style="width: 100%;">
                    <div style="width: 300px; float: left;">
                        <img src="images/img/spa/3.jpg" alt="3.jpg"/>
                    </div>
                    <div style="width: 500px; float: left;">
                        Acostumbrados a la medicina occidental con alopat&iacute;a o
                        medicamentos generales, es dif&iacute;cil tratar de
                        comprender la medicina oriental. Pero mientras la medicina tradicional mediante medicamentos farmac&eacute;uticos
                        tiene solo 150 años, la tradicional oriental cuenta con 5000 años de experiencia. Por estos
                        motivos tenemos que acercarnos a ella de una manera abierta y humilde y
                        comprobar la efectividad de los tratamientos e integrarlos en nuestro trabajo
                        terap&eacute;utico y personal a trav&eacute;s de la experimentaci&oacute;n.<br/><br/>
                    </div>
                </div>
                <div style="clear: both;"></div><br/>
                <div style="width: 100%; height: 250px;">
                    <div style="width: 205px; height: 200px; float: left;">
                        <img src="images/img/spa/27.jpg" alt="27.jpg" width="180" height="180"/>
                    </div>
                    <div style="width: 420px; height: 200px; float: left;">
                        El estudio de la medicina oriental es un viaje a trav&eacute;s de miles de años de
                        observaci&oacute;n y experimentaci&oacute;n que nos conduce a una mejor compresi&oacute;n
                        de la naturaleza humana y del mundo que nos rodea y nos proporciona una
                        herramienta para actuar con compasi&oacute;n sobre el sufrimiento y la enfermedad.
                    </div>
                    <div>
                        <img style="width: 150px; height: 200px; float: left;" src="images/img/spa/101.jpg" alt="101.jpg" width="180" height="180"/>
                    </div>
                </div>
                <div style="clear: both;"></div>

                <span class="titulo" style="float: right;">NUESTROS SERVICIOS</span><br/><br/>
                <span class="titulo">MEDICINA TRADICIONAL CHINA</span><br/><br/>
                La Medicina Tradicional China (MTC), medicina ancestral originaria de la antigua China, ha
                perdurado y se ha desarrollado a lo largo de la historia (más de 50 siglos) Los pilares básicos
                de la MTC son la Teoría del Yin y el Yang y la Teoría de los Cinco Elementos. Los principios
                y las premisas de la medicina china se extraen directamente de la filosofía tradicional taoista, la
                escuela del pensamiento más antigua y singular de China.<br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/4.jpg" alt="4.jpg" width="180" height="180"/>
                    <img src="images/img/spa/5.jpg" alt="5.jpg" width="150" height="180"/><br/><br/>
                </div>
                <span class="titulo">MEDICINA TRADICIONAL AYURVEDA</span><br/><br/>
                El ayurveda o aiur-veda1 es un antiguo sistema de medicina originado en la India y, de acuerdo a la
                doctora Margaret Chan (directora general de la Organización Mundial de la Salud), la Medicina Ayurvédica
                ―junto con la Medicina Tradicional China― son dos de los sistemas médicos vigentes más antiguos del 
                mundo. Practicada en India por los últimos cinco mil años, la Medicina Ayurvedica (que significa
                "ciencia de la vida") es un sistema comprensivo de la medicina que combina terapias naturales 
                con un acercamiento altamente personalizado al tratamiento de la enfermedad. La Medicina 
                Ayurvedica pone énfasis igual en cuerpo, mente, y espiritu, y se esfuerza de restablecer 
                la armonía natural del individuo, condición indispensable de la salud global. La clave de la
                Medicina Ayurvedica es la Constitución (Prakryti), y una vez identificada nos permite de establecer 
                el perfil total de la salud del individuo, incluyendo fuerzas y susceptibilidades de las personas para
                caer enfermos.<br/><br/>
                <span class="titulo">AROMATERAPIA</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/6.jpg" alt="6.jpg" width="180" height="180"/>
                    <img src="images/img/spa/7.jpg" alt="7.jpg" width="150" height="180"/>
                    <img src="images/img/spa/8.jpg" alt="8.jpg" width="150" height="180"/>
                    <img src="images/img/spa/9.jpg" alt="9.jpg" width="180" height="180"/>
                </div><br/>
                El termino Aromaterapia se invento gracias a un investigador franc&eacute;s, R-M Gattefos&eacute; (1881-
                1950) quien al quemarse la mano luego de sufrir una explosi&oacute;n en su laboratorio, tuvo el reflejo
                de sumergirla en un recipiente de aceite esencial de Lavanda (lav&aacute;ndula Angustifolia). Adem&aacute;s
                de sentir un alivio inmediato, la curaci&oacute;n y la cicatrizaci&oacute;n de la herida fue tan r&aacute;pida que
                Gattefos&eacute; se dedic&oacute; en adelante al estudio antibacteriano de los aceites esenciales.<br/><br/>
                <span class="titulo">CRISTALOTERAPIA / GEMOTERAPIA</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/10.jpg" alt="10.jpg" width="180" height="180"/>
                    <img src="images/img/spa/11.jpg" alt="11.jpg" width="150" height="180"/><br/><br/>
                </div>
                La Cristaloterapia o Gemoterapia Forma parte de los tratamientos vibracionales m&aacute;s eficaces
                para nivelar el flujo de nuestras energ&iacute;as, poni&eacute;ndonos en contacto directo con el subconsciente.
                La sanaci&oacute;n con cristales trabaja directamente con la luz, color, belleza y formas geom&eacute;tricas
                perfectas producidas en el reino mineral.<br/><br/>
                <span class="titulo">FLORES DE BACH</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/12.jpg" alt="12.jpg" width="240" height="180"/>
                    <img src="images/img/spa/13.jpg" alt="13.jpg" width="180" height="180"/>
                    <img src="images/img/spa/14.jpg" alt="14.jpg" width="220" height="180"/>
                </div><br/><br/>
                Flores de Bach, tambi&eacute;n llamadas remedios florales de Bach o esencias florales de Bach,
                es la denominaci&oacute;n gen&eacute;rica y comercial de un conjunto de 38 preparados naturales no
                farmacol&oacute;gicos, s&iacute;mil-homeop&aacute;ticos, elaborados a partir de una decocci&oacute;n o maceraci&oacute;n en agua
                de flores maduras de diversas especies vegetales silvestres o naturalizadas de la regi&oacute;n de Gales
                y la Inglaterra contigua, diluida en brandy (destilado del vino, usado como medio conservante.<br/><br/>
                <span class="titulo">GESTI&Oacute;N DE ESTR&Eacute;S</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/15.jpg" alt="15.jpg" width="180" height="180"/>
                    <img src="images/img/spa/16.jpg" alt="16.jpg" width="260" height="180"/>
                    <img src="images/img/spa/17.jpg" alt="17.jpg" width="180" height="180"/>
                </div><br/><br/>
                El estr&eacute;s es una respuesta natural y necesaria para la supervivencia, a pesar de lo cual hoy en d&iacute;a
                se confunde con una patolog&iacute;a. Esta confusi&oacute;n se debe a que este mecanismo de defensa puede
                acabar, bajo determinadas circunstancias frecuentes en ciertos modos de vida, desencadenando
                problemas graves de salud. Cuando esta respuesta natural se da en exceso se produce una
                sobrecarga de tensi&oacute;n que repercute en el organismo y provoca la aparici&oacute;n de enfermedades y
                anomal&iacute;as patol&oacute;gicas que impiden el normal desarrollo y funcionamiento del cuerpo humano.
                Algunos ejemplos son los olvidos (incipientes problemas de memoria),alteraciones en el &aacute;nimo,
                nerviosismo y falta de concentraci&oacute;n, entre otros s&iacute;ntomas.<br/><br/>
                <span class="titulo">MASOTERAPIA</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/18.jpg" alt="18.jpg" width="275" height="185"/>
                    <img src="images/img/spa/19.jpg" alt="19.jpg" width="275" height="185"/>
                </div><br/><br/>
                T&eacute;cnicamente, es un sistema que utiliza el Test Muscular como mecanismo de Bio-informaci&oacute;n,
                buscando en este estudio corregir los posibles desequilibrios que afectan a la persona. Son
                masajes variados, con piedras calientes con bolsas Pinda, o simplemente manuales.En un
                sentido m&aacute;s completo la masoterapiaes una forma de Comunicaci&oacute;n y estudio de todos los
                niveles que "conforman" al ser humano: F&iacute;sico, Qu&iacute;mico, Electromagn&eacute;tico, Emocional y
                Factor-X, tanto para saber comprender lo que est&aacute; pasando, como para conocer de qu&eacute; forma
                se pueden solucionar los diferentes problemas que padecemos. Un Masoterapeuta no hace
                diagn&oacute;sticos m&eacute;dicos, sino busca la causa del desequilibrio para que al solucionarse, se resuelva
                el s&iacute;ntoma. Es un Modelo Terap&eacute;utico que no lucha contra el s&iacute;ntoma, sino que refuerza y
                equilibra al sistema. El Masoterapeuta ayuda a su consultante a tomar conciencia del problema y
                su proceso, as&iacute; como de la explicaci&oacute;n de por qu&eacute; ha llegado a ese punto. Adem&aacute;s, le comunica
                desde su conocimiento m&aacute;s profundo para que el consultante decida lo que considera mejor para
                sanar su vida.<br/><br/>
                <span class="titulo">LINFOTERAPIA O DRENAJE LINFATICO</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/20.jpg" alt="20.jpg" width="230" height="180"/>
                    <img src="images/img/spa/21.jpg" alt="21.jpg" width="230" height="180"/>
                    <img src="images/img/spa/22.jpg" alt="22.jpg" width="230" height="180"/>
                </div><br/><br/>
                El sistema manual de drenaje de la Linfoterapia consigue recoger un fluido del cuerpo llamado
                linfa. La linfa est&aacute; presente en los tejidos del cuerpo y su funci&oacute;n es la de llevar los nutrientes
                a las c&eacute;lulas y recoger cualquier deshecho, sobrante, bacteria o material extraño de los tejidos.
                Mediante un masaje suave y sensitivo se activan los vasos linf&aacute;ticos ayud&aacute;ndoles a drenar
                las toxinas provenientes de una mala alimentaci&oacute;n, medicaci&oacute;n, etc., con lo que mejoran su
                actividad y la de los &oacute;rganos relacionados.<br/><br/>
                <span class="titulo">MASAJES</span><br/><br/>
                En la actualidad se coincide en definir al masaje como "una combinaci&oacute;n de movimientos
                t&eacute;cnicos manuales o maniobras realizadas armoniosa y met&oacute;dicamente, con fines higi&eacute;nico-
                preventivos y/o terap&eacute;uticos, que al ser aplicado con las manos permite valorar el estado de los
                tejidos tratados"; se emplea en medicina, kinesiolog&iacute;a, est&eacute;tica, deporte, etc.<br/><br/>
                <span class="titulo">MASAJES CON PIEDRAS CALIENTES</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/23.jpg" alt="23.jpg" width="325" height="155"/>
                    <img src="images/img/spa/24.jpg" alt="24.jpg" width="192" height="155"/>
                </div><br/><br/>
                El masaje con piedras calientes y fr&iacute;as no es nuevo. Desde hace muchos siglos, tanto la
                cultura China, como la de los indios americanos, utilizaban las piedras calientes para
                dar masaje y aliviar dolencias. Desde los puntos chakra, la energ&iacute;a es distribuida a todo
                el organismo, gracias a los numerosos canales energ&eacute;ticos que lo atraviesan y que est&aacute;n
                unidos a ellos. Concentrarse sobre los chakra ayuda, por lo tanto, a regular el flujo
                energ&eacute;tico en el cuerpo (demasiada energ&iacute;a en c&iacute;rculo o demasiado poca puede crear
                trastornos) . Precisamente, con esta finalidad, durante la "stone teraphy". las piedras
                calientes se disponen sobre los chakra y, a continuaci&oacute;n, se procede a la "apertura" y
                el "cierre" energ&eacute;ticos y a la conexi&oacute;n, tambi&eacute;n energ&eacute;tica, de los miembros superiores
                e inferiores.El elemento fundamental de la terapia geotermal, lo constituye la aplicaci&oacute;n
                de piedras calientes y fr&iacute;as que act&uacute;an a dos niveles:
                A) De forma est&aacute;tica, ejerciendo presiones terap&eacute;uticas en puntos concretos
                B) De forma din&aacute;mica realizando maniobras de masaje bien definidas
                "Las piedras calientes (50°) incrementan el riego sangu&iacute;neo y el metabolismo celular,
                mientras que las fr&iacute;as ( 8°) originan vasoconstricci&oacute;n y liberaci&oacute;n de histamina que
                act&uacute;a sobre el dolor y los procesos inflamatorios".<br/><br/>
                <span class="titulo">MASAJES CON PINDAS</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/25.jpg" alt="25.jpg" width="180" height="180"/>
                    <img src="images/img/spa/26.jpg" alt="26.jpg" width="180" height="180"/>
                    <img src="images/img/spa/27.jpg" alt="27.jpg" width="180" height="180"/>
                    <img src="images/img/spa/28.jpg" alt="28.jpg" width="180" height="180"/>
                </div><br/><br/>
                El masaje con pindas es un tratamiento ayurv&eacute;dico inspirado en el masaje thai cuya finalidad
                es el bienestar y relajaci&oacute;n del cuerpo y la mente. Sus maniobras proporcionan beneficios que
                depender&aacute;n de las plantas que se utilicen, y es parte de una forma de vida que poco a poco se
                ha ido introduciendo en Am&eacute;rica y que ha permitido a los terapeutas trabajar de una forma m&aacute;s
                hol&iacute;stica.<br/><br/>
                <span class="titulo">MEDITACIONES ACTIVAS</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/29.jpg" alt="25.jpg" width="180" height="180"/>
                    <img src="images/img/spa/30.jpg" alt="26.jpg" width="230" height="180"/>
                    <img src="images/img/spa/31.jpg" alt="27.jpg" width="260" height="180"/>
                </div><br/><br/>
                Hasta ahora se pensaba que las t&eacute;cnicas de meditaci&oacute;n era dif&iacute;ciles, aburridas, poco asequibles
                y casi un patrimonio de los m&iacute;sticos. Sin embargo con las nuevas y modernas Meditaciones
                Activas, el hecho de meditar resulta una experiencia f&aacute;cil, sencilla y hasta divertida.
                Especialmente adaptadas a nuestra cultura occidental, todos podemos no s&oacute;lo practicarlas y
                gozar de sus beneficios, sino que ahora incluso dirigirlas y ayudar a los dem&aacute;s a encontrarse a s&iacute;
                mismos para ser m&aacute;s conscientes y felices. No es necesario adoptar la posici&oacute;n tradicional. Se
                puede realizar simplemente sentado en una silla.<br/><br/>
                <span class="titulo">MOXIBUSTI&Oacute;N</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/32.jpg" alt="32.jpg" width="250" height="180"/>
                    <img src="images/img/spa/33.jpg" alt="33.jpg" width="170" height="180"/>
                    <img src="images/img/spa/34.jpg" alt="34.jpg" width="250" height="180"/>
                </div><br/><br/>
                Este es un tipo de terapia que consiste en aplicar calor en varias partes del cuerpo para
                restablecer y equilibrar el flujo energ&eacute;tico del organismo. La moxibusti&oacute;n es una pr&aacute;ctica de
                la medicina oriental. Es un tratamiento que utiliza los mismos puntos que la acupuntura,
                pero a diferencia de &eacute;sta, utiliza el calor como herramienta para aliviar tensiones. El calor es
                transmitido al quemar una hierba denominada "moxa" obtenida de la planta conocida con el
                nombre de Artemisa (Artemisia vulgaris ). El calor nunca toca la piel, por lo cual nunca quema.<br/><br/>
                <span class="titulo">REFLEXOLOGIA</span><br/><br/>    
                Reflexología o terapia zonal es la práctica de estimular puntos sobre los pies, manos, u orejas
                (llamados zonas de reflejo), con la esperanza de que tendrá un efecto benéfico sobre otras
                partes del cuerpo, o de que mejorará la salud general. La Reflexología trabaja el Sistema
                Nervioso a través de los dermatomas. Nuestro cuerpo esta provisto de terminaciones nerviosas 
                en el toda la superficie, siendo los pies y las manos los que ocupan una área más amplia respecto 
                a las demás partes del cuerpo.<br/><br/>
                <span class="titulo">REFLEXOLOGIA FACIAL</span><br/><br/>    
                <div class="img-center">
                    <img src="images/img/spa/35.jpg" alt="35.jpg" width="180" height="180"/>
                    <img src="images/img/spa/36.jpg" alt="36.jpg" width="270" height="180"/>
                    <img src="images/img/spa/37.jpg" alt="37.jpg" width="270" height="180"/><br/><br/>
                </div>
                Sin necesidad de las palabras, el rostro puede llegar a transmitir los sentimientos más secretos,
                incluso los que desconocemos. Nuestro rostro comprende más de 500 puntos interconectados con todo 
                el cuerpo. La reflexología facial, terapia introducida por Lone Sorensen, permite tratar estos puntos,
                actuando sobre la totalidad del sistema nervioso. Igualmente, cuando se presentan alteraciones de 
                la salud, el semblante comienza a transformarse silenciosamente, evidenciando los trastornos que
                experimenta el organismo, por lo que para muchos la cara puede llegar a representar la totalidad 
                corporal, tanto física como emocionalmente, siendo además la única zona del cuerpo donde se encuentran 
                todos los órganos de los sentidos.<br/><br/>
                <span class="titulo">REFLEXOLOGIA PODAL</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/38.jpg" alt="38.jpg" width="140" height="180"/>
                    <img src="images/img/spa/39.jpg" alt="39.jpg" width="300" height="190"/>
                    <img src="images/img/spa/40.jpg" alt="40.jpg" width="300" height="190"/>
                </div><br/><br/>
                En la actualidad, prestamos muy poca importancia a los pies, es el gran olvidado de
                nuestro cuerpo y sin embargo, es la parte del cuerpo donde van a parar multitud de
                terminaciones nerviosas.
                La planta del pie y el pie en su conjunto, representa en forma esquem&aacute;tica la totalidad
                del cuerpo y sus &oacute;rganos. Este es el principio de la reflexolog&iacute;a podal.
                En ella, mediante el masaje se puede influir a modo reflejo sobre los correspondientes
                &oacute;rganos y v&iacute;sceras, aparte de la importancia diagn&oacute;stica que tiene.
                Los buenos resultados de la reflexolog&iacute;a podal y la escasez de efectos secundarios, hace
                que hoy en d&iacute;a, sea aplicada con m&aacute;s frecuencia por los profesionales sanitarios, entre
                ellos los fisioterapeutas.
                La idea de que manipulando los pies con las manos y las yemas de los dedos se pueden
                estimular las diferentes zonas del organismo, facilitando la curaci&oacute;n y previniendo la
                enfermedad, ya formaba parte de muchas terapias m&eacute;dicas de la antigüedad, desde el Egipto
                cl&aacute;sico a la Antigua India.<br/><br/>
                <span class="titulo">REGISTROS AKASHICOS</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/41.jpg" alt="41.jpg" width="180" height="180"/>
                    <img src="images/img/spa/42.jpg" alt="42.jpg" width="180" height="180"/>
                    <img src="images/img/spa/43.jpg" alt="43.jpg" width="180" height="180"/><br/><br/>
                </div>
                Los registros akáshicos (de akasha, en sánscrito: cielo, espacio, éter) son una especie de memoria
                de todo lo que ha acontecido desde el inicio de los tiempos que estaría registrada en el éter. Allí
                se almacenaría todo lo que ha acontecido desde el inicio de los tiempos y todos los conocimientos
                del universo. Es un sistema que, a diferencia de la sofrosis médica, no utiliza la hipnosis.<br/><br/>
                <span class="titulo">REIKI USUI</span><br/><br/>
                <div class="img-center">        
                    <img src="images/img/spa/44.jpg" alt="44.jpg" width="190" height="190"/>
                    <img src="images/img/spa/45.jpg" alt="45.jpg" width="259" height="190"/>
                    <img src="images/img/spa/46.jpg" alt="46.jpg" width="259" height="190"/>
                </div><br/><br/>
                La pr&aacute;ctica de Reiki consiste en la canalizaci&oacute;n de Energ&iacute;a Universal o espiritual para armonizar
                el cuerpo, la mente y el esp&iacute;ritu, creando una persona sana y equilibrada. El Reiki es una t&eacute;cnica
                de armonizaci&oacute;n natural a traves de la Energ&iacute;a Vital Universal.
                El origen exacto de Reiki se desconoce ya que se pierde en los anales de los tiempos y el
                conocimiento de esta t&eacute;cnica hubiese quedado perdida para siempre si no hubiese sido por un
                monje cristiano llamado Mikao Usui que a finales del siglo XIX, redescubre la t&eacute;cnica y a partir
                de ese momento comienza a difundirla.
                Reiki es un t&eacute;cnica Hol&iacute;stica ya que armoniza todos los planos del ser humano, armoniza el
                plano f&iacute;sico, el plano mental, el plano emocional y el plano espiritual. Reiki toma al ser humano
                como un todo por lo tanto lo armoniza en conjunto. Reiki armoniza a trav&eacute;s de la Energ&iacute;a
                Vital Universal por lo tanto es una t&eacute;cnica natural que no agrede al cuerpo de ninguna manera
                y tampoco crea adicci&oacute;n ni efectos secundarios o colaterales ya que no se utilizan sustancias
                qu&iacute;micas ni elementos extraños al cuerpo sino solo la energ&iacute;a de vida que est&aacute; presente en todo
                ser vivo.
                Al ser armonizados con la misma energ&iacute;a que nos sustenta nos estamos conectando con nuestra
                fuente de vida, nos conectamos con esa energ&iacute;a Universal maravillosa que a todo ser viviente le
                da la posibilidad de existir y ser. Debido a que Reiki es una terapia natural y hol&iacute;stica puede ser
                utilizada en cualquier enfermedad o desequilibrio que una persona pueda llegar a tener. Reiki se
                puede, y de hecho es recomendable, utilizar con el tratamiento medico tradicional pero nunca
                deber&aacute; reemplazar a &eacute;ste. Recordemos que Reiki es una terapia que complementa el tratamiento
                medico pero no lo sustituye. Es por eso que se recomienda tomar sesiones de Reiki como
                tratamiento de prevenci&oacute;n como m&iacute;nimo un vez por semana, aunque esto lo tendr&aacute; que analizar
                el reikista viendo la situaci&oacute;n del paciente.<br/><br/>
                <span class="titulo">RELAJACI&Oacute;N</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/47.jpg" alt="47.jpg" width="282" height="175"/>
                    <img src="images/img/spa/48.jpg" alt="48.jpg" width="295" height="175"/>
                    <img src="images/img/spa/49.jpg" alt="49.jpg" width="200" height="175"/>
                </div><br/><br/>
                El estilo de vida actual, y sobretodo, en aglomeraciones urbanas, hay una serie de situaciones
                añadidas, que pueden convertirse en agentes estresantes como: el ruido ambiental, el tr&aacute;fico
                lento, la alta competitividad, la conciliaci&oacute;n de la vida laboral y profesional… Todo ello nos
                puede llevar a situaciones de estr&eacute;s. Cada vez se hace m&aacute;s imprescindible pues, la pr&aacute;ctica
                diaria de alguna t&eacute;cnica de relajaci&oacute;n, hasta convertirla en un h&aacute;bito, en parte de nuestra rutina
                diaria.<br/><br/>
                <span class="titulo">SHIATSU</span><br/><br/>
                <div class="img-center">
                    <img src="images/img/spa/50.jpg" alt="50.jpg" width="260" height="190"/>
                    <img src="images/img/spa/51.jpg" alt="51.jpg" width="260" height="190"/>
                    <img src="images/img/spa/52.jpg" alt="52.jpg" width="260" height="190"/>
                </div>      <br/><br/>
                El Shiatsu es una terapia manual de origen japon&eacute;s, utiliza presiones, estiramientos,
                movilizaciones y conexiones. Trata el cuerpo en su totalidad estimulando y favoreciendo su
                reencuentro con un estado de equilibrio y armon&iacute;a natural. El Objetivo del Shiatsu es permitir
                que el K&iacute;, la energ&iacute;a vital de todo ser vivo, fluya sin obst&aacute;culos por la sutil red de meridianos
                energ&eacute;ticos. Los desequilibrios de la salud aparecen cuando el K&iacute; se obstruye, provocando toda
                una serie de s&iacute;ntomas y signos.<br/><br/>

                <span class="titulo">RESUMEN DE NUESTROS SERVICIOS DE SPA:</span><br/><br/>
                <div style="text-align: center">
                    *Armonizaci&oacute;n Reiki con Cuenco Tibetano.<br/>
                    *Masaje Relajante y Descontracturante mixto manual y con Piedras.<br/>
                    *Reiki Usui (Tradicional.<br/>
                    *Aromaterapia.<br/>
                    *Reflexolog&iacute;a Integral.<br/>
                    *Masaje con piedras calientes.<br/>
                    *Masaje Hind&uacute; Sweda con Pindas (Ayurveda).<br/>
                    *Masaje Oriental Integral.<br/>
                    *Masaje Facial Energizante.<br/>
                    *Shiatsu.<br/>
                    *Masajes Descontracturantes y de relajaci&oacute;n.<br/>
                    *Gemoterapia con Piedras Semipreciosas y Elixir.<br/>
                    *Moxibusti&oacute;n.<br/>
                    *Flores de Bach con Medicamentos y Energizaci&oacute;n.<br/>
                    *Linfoterapia (drenaje linf&aacute;tico).<br/>
                    *Masaje Reflexol&oacute;gico Podal manual.<br/>
                    *Meditaci&oacute;n Akashica Activa.<br/>
                    *Registros Akashicos.<br/>
                    *Gesti&oacute;n de Estr&eacute;s.<br/>
                    *Psicolog&iacute;a Cognitiva. Conductual.<br/><br/>
                </div>
            </div>
        </div>


<?php include 'footer.php'; ?>