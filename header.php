<?php
header('Content-type: text/html; charset=utf-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>La Posada del Padre Pio</title>
        <meta name="keywords" content="Hotel, Cosquin, Padre, Pio, reservas, vacaciones, cordoba, argentina"/>
        <meta name="author" content="Ignacio Gonzalez"/>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" type="image/png" href="images/fav.ico"/>

        <!--
        <link href='http://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        -->
        
        <!--<link rel="stylesheet" type="text/css" href="pre-styles/jquery.mobile-1.4.5.min.css"/>-->
        <link rel="stylesheet" type="text/css" href="pre-styles/jquery.lightbox-0.5.css"/>
        <link rel="stylesheet" type="text/css" href="pre-styles/estilos.css"/>
        <link rel="stylesheet" type="text/css" href="pre-styles/responsive-nav.css"/>
        <link rel="stylesheet" type="text/css" href="pre-styles/styles.css"/>
        
        <!--
        <link rel="stylesheet" type="text/css" href="css/all.css"/>
        -->        

        <script type="text/javascript" src="pre-scripts/responsive-nav.min.js"></script>
        <script type="text/javascript" src="pre-scripts/jquery-1.11.3.min.js"></script>
        <!--<script type="text/javascript" src="pre-scripts/jquery.mobile-1.4.5.min.js"></script>-->
        <script type="text/javascript" src="pre-scripts/jquery.lightbox-0.5.js"></script>
        <script type="text/javascript" src="pre-scripts/script.js"></script>
        

        <!--
        <script type="text/javascript" src="scripts/all.js"></script>
        -->        

    </head>
    <body>
        <!--
        <div style="width: 100%; background-color: #666666;">
            <div class="fecha">Bienvenido, hoy es <?php #echo date("Y-m-d G:i"); ?></div>
        </div>
        -->

    <nav class="nav-collapse">
      <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="fotos.php">Im&aacute;genes</a></li>
        <li><a href="contacto.php">Contacto</a> </li>
        <li><a href="tarifas.php">Tarifas</a></li>
        <li><a href="ubicacion.php">Ubicaci&oacute;n</a></li>
        <li><a href="servicios.php">Servicios</a></li>
        <li><a href="spa.php">Area Spa</a></li>
        <li><a href="pio.php">Padre P&iacute;o</a></li>
        <li><a href="video.php">Video</a></li>
      </ul>
    </nav>

    <div style="height:55px; background-color:#fff;"></div>

    <div style="clear:both;"></div>

    <div style="display: table; overflow: hidden; width: 100%;">
        <div class="direccion" style:"font-family: 'Open Sans', sans-serif;">
           La posada del padre pio Sarmiento 168 (5166) Cosquin, Cordoba.
        </div>
    </div>
        
    <div style="clear:both;"></div>

