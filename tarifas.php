<?php require_once 'clases/db.class.php';

$sql = "SELECT * FROM  precios WHERE idprecios = 0;";
$db = new db();
$db->connect();
$consulta = $db->ejecutar($sql);

include 'header.php'; 

?>

<div class="pagina"><br/>
    <div class="marco-secciones"><br/>
        <div class="titulo">Tarifas</div><br/>

        <div>
            <b><font color="#61250B" size="4">Hotel "La Posada del Padre P&Iacute;o" Sarmiento 168. Cosquín. Córdoba</font></b>            
        </div><br/>

        <div class="off">
            <img style="border: solid 1px #FF9900;" src="images/tarifas2.png" alt="images/tarifas2.png" width="180" height="132"/>
        </div>

        <div style="clear: both;"></div><br/>

        <div class="promo">CONSULTAR PROMOCIONES PARA GRUPOS (NO ESTUDIANTILES)</div><br/>
        <div class="center"><b>Tarifas a&ntilde;o 2015</b></div><br/>
        <div style="width: 100%; text-align: center;">
            <table style="width: 100%; text-align: center;">
                <tr>
                    <td><b>Habitaci&oacute;n</b></td>
                    <td><b>Temporada baja</b></td>
                    <td><b>Temporada media</b></td>
                    <td><b>Temporada alta</b></td>
                </tr>
                    <?php while ($res = mysql_fetch_array($consulta)) { ?>
                    <tr>
                        <td>Doble</div>
                        <td><?php echo $res["db"]; ?></td>
                        <td><?php echo $res["dm"]; ?></td>
                        <td><?php echo $res["da"]; ?></td>
                    </tr>
                    <tr>
                        <td>Triple</div>
                        <td><?php echo $res["tb"]; ?></td>
                        <td><?php echo $res["tm"]; ?></td>
                        <td><?php echo $res["ta"]; ?></td>
                    </tr>
                    <tr>
                        <td>Cu&aacute;druple</td>
                        <td><?php echo $res["cb"]; ?></td>
                        <td><?php echo $res["cm"]; ?></td>
                        <td><?php echo $res["ca"]; ?></td>
                    </tr>
                    <?php } ?>
            </table>

            <div style="clear: both;"></div><br/>
            <div style="text-align: center; width: 100%;">
                <b>* Temporada alta</b>: 23-12-11 al 29-02-12<br/>
                <b>* Temporada media</b>: Fin de semanas largos, Semana Santa, Vacaciones de invierno<br/>
                <b>* Temporada Baja</b>: resto del año.<br/>
                <b>* Los precios se encuentran expresados en pesos argentinos. Los mismos pueden ser modificados sin previo
                aviso de acuerdo a las condiciones económicas del mercado.</b>
            </div><br/>
            <p class="mail">
                <a href="mailto:laposadadelpadrepio@hotmail.com.ar">laposadadelpadrepio@hotmail.com.ar</a>
            </p>
            Tel. 03541 454706 - Tel. 03541 - 15380396<br/>
        </div><br/>
    </div>
</div>

<?php include 'footer.php'; ?>