<?php include 'header.php'; ?>

        <div class="pagina">
            <div class="marco-secciones"><br/>
                <div class="titulo">Servicios</div><br/>
                <span>Nuestra moderna estructura le ofrece 21 habitaciones distribuidas en 2 pisos. Habitaciones dobles,
                    triples y cuadruples con o sin cama matrimonial (camas sommier).</span><br/><br/>
                <!--<div style="float: left; padding-left: 25px; padding-right: 75px;">
                    <img src="images/cosquin1.jpg" width="200px" height="141px" alt="images/cosquin1.jpg" class="active" /><br/>
                    <img src="images/cosquin2.jpg" width="200px" height="141px" alt="images/cosquin2.jpg"/><br/>
                    <img src="images/cosquin3.jpg" width="200px" height="141px" alt="images/cosquin3.jpg"/><br/>
                </div>-->   
                En todos los casos contar&aacute; con los siguientes servicios:<br/><br/>
                    <div class="servicios-left">
                        - Recepci&oacute;n las 24 horas<br/>
                        - Reservas anticipadas<br/>
                        - Servicio de mucama<br/>
                        - Sala de estar<br/>
                        - Desayuno Buffet<br/>
                        - Room service<br/>
                        - Hermosa vista al cerro Pan de Az&uacute;car <br/>
                        - Ventiladores de techo y calefacci&oacute;n<br/>
                        - Todas las habitaciones disponen de ba&ntilde;o privado<br/>
                        - Servicios de bar las 24 hs.<br/>
                        - Cajas de seguridad.<br/>
                        - Música funcional.<br/>
                        - Internet / WIFI<br/>
                        - Emergencias m&eacute;dicas<br/>
                        - Estacionamiento propio<br/>
                        - A solo 30 metros del r&iacute;o Cosqu&iacute;n<br/><br/>
                    </div>
                    <div class="servicios-right">
                        SERVICIOS OPCIONALES<br/><br/>
                        - Servicio de lavander&iacute;a/valet.<br/>
                        - Servicio de Remises<br/>   
                        - Servicios complementarios:<br/>
                        - Sector de Terapias Orientales Integrales:<br/>
                        - Reiki:<br/>
                        - Utsui (tradicional)<br/>
                        - Tibetano (con cuenco).<br/>
                        - Masajes descontracturantes y de relajación:<br/>
                        - Shiatsu<br/>
                        - Reflexología Podal<br/>
                        - Auriculorterápia<br/>
                        - Musicoterapia, Maxibustión<br/>
                        - Meditación asistida.<br/>
                    </div>
            </div>
            <div style="clear: both;"></div>
        </div><br/><br/>
        
<?php include 'footer.php'; ?>