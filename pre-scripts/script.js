$(document).ready(function(){
   $('#gallery a').lightBox();
});

function subir_comentario()
{
    var err = 0;
    var email = document.getElementById("email");
    var comentario = document.getElementById("comentario");
    var erremail = document.getElementById("error-email");
    if (email.value.length==0){
        email.style.background = '#ff3300';
        err = 1;
    }
    if (comentario.value.length==0){
        comentario.style.background = '#ff3300';
        err = 1;
    }
    var parametros = "";
    parametros += "&t0="+email.value + "&t1=" +comentario.value;
    if(err == 0){
        $.ajax
        ({
            type:"POST",
            url:"mail.php",
            data: parametros,
            success: function(datos)
            {
                alert('Su mensaje ha sido enviado.');
            }
        })
        err = 0;
        email.style.background = '#ffffff';
        comentario.style.background = '#ffffff';
        email.value = "";
        comentario.value = "";
        erremail.innerHTML = "";
    }else{
        alert("Complete todos los campos!")
    }
}

function comprobarNumero(str,diverror){
    var myRegxp = /^[0-9]+$/; //solo numeros
    if (myRegxp.test(str)==false)
        if ( str =="")
            mostrarError(7,diverror);
        else
            mostrarError(2,diverror);
    else
        mostrarError(0,diverror);
}
function mostrarError(codigoerror,diverror){
    var errores = new Array ("","Usa solo letras.","Usa solo numeros.","Usa el formato nombre@ejemplo.com.",
        "Ingresa al menos 2 caracteres.","Ingresa al menos 6 caracteres.",
        "Las claves son diferentes","El campo esta vacio");
    get(diverror).innerHTML = errores[codigoerror];
}
function comprobarTexto(str,diverror){
    var myRegxp = /^[a-zA-Z\s]+$/; //solo letras
    if (myRegxp.test(str)==false)
        if ( str =="")
            mostrarError(7,diverror);
        else
            mostrarError(1,diverror);
    else
        mostrarError(0,diverror);
}
function comprobarClave(diverror){
    var clave = (get("clave"))?get("clave").value:0;
    var repiteclave = (get("repiteclave"))?get("repiteclave").value:0;
    if( clave != repiteclave)
        mostrarError(6,diverror);
    else
        mostrarError(0,diverror);
}
function comprobarEmail(str,diverror){
    var myRegxp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/; //comprobar Email
    if (myRegxp.test(str)==false)
        if ( str =="")
            mostrarError(7,diverror);
        else
            mostrarError(3,diverror);
    else
        mostrarError(0,diverror);
}

function updatePrecios(){
    var parametros = "";
    var db = document.getElementById("db");
    var dm = document.getElementById("dm");
    var da = document.getElementById("da");
    var tb = document.getElementById("tb");
    var tm = document.getElementById("tm");
    var ta = document.getElementById("ta");    
    var cb = document.getElementById("cb");
    var cm = document.getElementById("cm");
    var ca = document.getElementById("ca");
    var parametros = "";
    parametros += "t0="+ db.value + "&t1=" + dm.value + "&t2=" + da.value + "&t3=" +tb.value + 
    "&t4=" +tm.value + "&t5=" + ta.value + "&t6=" + cb.value + "&t7=" + cm.value + "&t8=" + ca.value;
    $.ajax({
        type:"POST",
        url:"actualizar.php",
        data: parametros,
        error: function(xhr, status, error) {
            // Show the error
            alert(xhr.responseText + status + error);
        },
        success: function(datos){
            if(datos)
                alert('Precios actualizados.');
            else
                alert('Hubo algun error, contactarse con nacho.');
        }
    })
}