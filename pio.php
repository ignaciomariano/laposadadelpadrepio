<?php include 'header.php'; ?>


        <div class="pagina">
            <div class="marco-secciones"><br/>
                <p style="text-align: center;">
                    <img src="images/tarifas1.png" alt="images/tarifas1.png" width="128" height="163"/><br/>
                </p>
                <b>Francesco Forgione (Pío de Pietrelcina O. F. M. Cap.)</b> también conocido como <b>Padre Pío</b>.
                Es uno de los santos más venerados en Italia <b>y de todo el orbe</b>.
                Muchos católicos de todo el mundo llevan su imagen en la cartera,
                la guardan en sus despachos o la exhiben en el automóvil colgado de los espejos retrovisores.<br/><br/>
                El Padre Pío nació en Pietrelcina, una aldea en la sureña Benevento, el 25 de mayo de 1887.
                Consagrado sacerdote el 10 de Agosto de 1910 en la catedral de Benevento, por motivos de salud
                permaneció con su familia hasta 1916. <b>Allí en su pueblo natal recibe los primeros estigmas</b>.
                En septiembre del mismo año fue enviado al convento de San Giovanni Rotondo y permaneció allí hasta su muerte.<br/><br/>
                <b>Los dones probados del sacerdote italiano fueron:</b><br/><br/>
                <b>Discernimiento extraordinario</b>: capacidad de leer las conciencias, don que utilizó
                frecuentemente durante el ministerio del sacramento de la Penitencia.<br/>
                <b>Profecía</b>: pudo anunciar eventos del futuro, incluida la llegada al papado de Pablo VI y de Juan Pablo II.<br/>
                <b>Curación</b>: curas milagrosas por el poder de la oración.<br/>
                <b>Bilocación</b>: estar en dos lugares al mismo tiempo.<br/>
                <b>Perfume</b>: la sangre de sus estigmas tenían fragancia de flores.<br/>
                <b>Estigmas</b>: recibió los estigmas el 20 de septiembre de 1918 y los llevó hasta su muerte 50 años después.<br/>
                Los médicos que observaron los estigmas del Padre Pío no pudieron hacer cicatrizar sus llagas ni dar explicación de ellas.
                El dolor físico que se notaba estaba relacionado con los estigmas visibles y sangrantes que tenía.
                Eran las marcas de la Pasión de Cristo en su cuerpo, las cuales le causaban un inmensísimo dolor físico
                y una terrible incomodidad.<br/><br/>
                Dicen los frailes capuchinos que lo asistían,
                que sólo de la herida del pecho fluía una cantidad de sangre equivalente  más o menos a una taza.  Y esto, diariamente. 
                Los estigmas eran tal vez la gracia extraordinaria más notoria en el Padre Pío, que se podían apreciar aun a pesar
                de los mitones marrones que llevaba siempre puestos, menos en el momento de la Consagración.<br/><br/>

                El Padre Pío fue beatificado el 2 de Mayo de 1999 y el 16 de Junio de 2002 el Papa Juan Pablo II
                lo canonizó (proclamó santo) bajo el nombre de <b>San Pío de Pietrelcina</b>.<br/><br/>
                <p style="text-align: center;">
                    <img src="images/pio2.png" alt="images/pio2.png" width="264" height="190"/>
                    <img src="images/pio3.png" alt="images/pio3.png" width="240" height="190"/>
                </p>
                <br/>
                <div style="clear: both;"></div>
                El  24 de Abril de 2008 su cadáver fue expuesto 40 años después de su muerte en el santuario
                de Santa María de la Gracia, de San Giovanni Rotondo, donde su cuerpo permanece en estado incorrupto.
            </div><br/>
        </div>

<?php include 'footer.php'; ?>








<!--    <b>Francesco Forgione  (P&iacute;o de Pietrelcina O. F. M. Cap.,)</b> tambi&eacute;n conocido como <b>Padre P&iacute;o</b> (Pietrelcina, 25 de mayo de 1887
– San Giovanni Rotondo, 23 de septiembre de 1968) fue un religioso capuchino y santo italiano. Famoso por los hechos
sobrenaturales que se le atribuyen, como estigmas, curaciones, bilocaciones y lectura de conciencias a los que iban a
confesarse con &eacute;l.
Consagrado sacerdote el 10 de agosto de 1910 en la catedral de Benavento, por motivos de salud permaneci&oacute; en su familia
hasta 1916. <b>All&iacute; en su pueblo natal recibe los primeros estigmas</b>. En septiembre del mismo a&ntilde;o fue enviado al convento de
San Giovanni Rotondo y permaneci&oacute; all&iacute; hasta su muerte.
Sin duda alguna lo que ha hecho m&aacute;s famoso al Padre P&iacute;o es el fen&oacute;meno de los estigmas llamados pasionarios
(por ser semejantes a los de Jesucristo en su Pasi&oacute;n): heridas en manos, pies, costado y hombro, dolorosas aunque
invisibles entre 1911 y 1918, <b>y visibles desde este &uacute;ltimo a&ntilde;o hasta su muerte</b>. Su sangre ten&iacute;a al parecer perfume
de flores, aroma asociado a la santidad. La noticia de que el Padre P&iacute;o ten&iacute;a los estigmas se extendi&oacute; r&aacute;pidamente. Muy pronto miles de personas acud&iacute;an a San Giovanni Rotondo para verle, besarle sus manos, confesarse con &eacute;l y asistir a sus misas. Se trata del primer sacerdote estigmatizado.
Al morir desaparecieron los estigmas que padeci&oacute; durante gran parte de su vida a los que se atribuyen, un origen
m&iacute;stico y sobrenatural.
Se le atribuyen numerosas sanaciones y conversiones concedidas por la intercesi&oacute;n del Padre P&iacute;o y otros milagros
han sido reportados al Vaticano.
Los dones del sacerdote italiano fueron, seg&uacute;n su proceso de canonizaci&oacute;n:<br/>
-Discernimiento extraordinario: capacidad de leer las conciencias, don que utiliz&oacute; frecuentemente durante el ministerio
del sacramento de la Penitencia.<br/>
- Profec&iacute;a: pudo anunciar eventos del futuro, incluida la llegada al papado de Pablo VI y de Juan Pablo II.<br/>
- Curaci&oacute;n: curas milagrosas por el poder de la oraci&oacute;n.<br/>
- Levitaci&oacute;n durante la Consagraci&oacute;n del pan y vino.<br/>
- Bilocaci&oacute;n: estar en dos lugares al mismo tiempo.<br/>
- Perfume: la sangre de sus estigmas ten&iacute;an fragancia de flores.<br/>
- L&aacute;grimas: comprend&iacute;a los misterios del Rosario hasta las l&aacute;grimas.<br/>
- Estigmas: recibi&oacute; los estigmas el 20 de septiembre de 1918 y los llev&oacute; hasta su muerte 50 a&ntilde;os despu&eacute;s. Los m&eacute;dicos que
observaron los estigmas del Padre P&iacute;o no pudieron hacer cicatrizar sus llagas ni dar explicaci&oacute;n de ellas. Calcularon que
perd&iacute;a una copa de sangre diaria, pero sus llagas nunca se infectaron.<br/>
El Padre P&iacute;o fue beatificado el 2 de mayo de 1999 y el 16 de junio del 2002, Juan Pablo II lo canoniz&oacute; bajo el nombre de
San P&iacute;o de Pietrelcina.
El 24 de Abril de 2008, 40 a&ntilde;os despu&eacute;s de su muerte, el cad&aacute;ver de Padre P&iacute;o fue exhumado y exhibido p&uacute;blicamente, el
cual, para sorpresa de todos los presentes, se mostr&oacute; y mantiene totalmente incorruptible.<br/><br/>
-->
