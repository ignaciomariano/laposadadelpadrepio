<?php include 'header.php'; ?>

        <div class="pagina">
            <div class="marco-secciones"><br/>
                <div class="titulo">Ubicaci&oacute;n</div><br/>
                <div>
                    <div style="text-align: center;">
                        <img src="images/img/secciones/ubicacion.jpg" width="70%" /><br/><br/>
                    </div>
                    <!--
                    <div style="float: left; width: 430px; height: 360px; padding-right: 30px; padding-top: 5px;">
                        <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                                src="http://maps.google.com.ar/?ie=UTF8&amp;ll=-31.244737,-64.459491&amp;spn=0.003651,0.005815&amp;t=h&amp;z=17&amp;
                                vpsrc=6&amp;output=embed">
                        </iframe>
            
                    <!--<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com.ar/maps?f=q&amp;source=s_q&amp;hl=es-419&amp;geocode=&amp;q=Sarmiento+168,+C%C3%B3rdoba&amp;aq=0&amp;sll=-37.439974,-63.28125&amp;sspn=35.066761,79.013672&amp;ie=UTF8&amp;hq=&amp;hnear=Sarmiento+168,+C%C3%B3rdoba&amp;z=14&amp;iwloc=a&amp;output=embed">
                    </iframe>-->
                </div>
                El hotel se encuentra ubicado en calle Sarmiento al 168, a solo 30 metros del R&iacute;o Cosqu&iacute;n, a 7 cuadras
                de la Plaza Pr&oacute;spero Molina (del folklore) y 5 cuadras de la Terminal de &Oacute;mnibus.<br/><br/>
                Capital del turismo, la m&uacute;sica y la familia, la ciudad de Cosqu&iacute;n congrega a&ntilde;o tras a&ntilde;o miles de turistas
                ansiosos por disfrutar del sol, la playa y el aire serrano. Un clima ideal, originado por la ubicaci&oacute;n de esta
                localidad en el centro del Valle de Punilla, la hace propicia para el disfrute pleno en cualquier per&iacute;odo estacional.
                Reconocida por sus valiosos eventos art&iacute;stico- culturales, entre los que sobresalen el Festival Nacional de Folklore,
                y el mayor acontecimiento rockero del pa&iacute;s; la ciudad de Cosqu&iacute;n refuerza la seducci&oacute;n de sus bellezas naturales con
                servicios de primer nivel en gastronom&iacute;a y entretenimiento, capaces de satisfacer las expectativas de los cientos y
                cientos de visitantes.<br/><br/>
                El puente carretero y las tradicionales aerosillas facilitan el acceso a la magnifica vista desde la cima del Cerro
                Pan de Az&uacute;car, que se ha convertido en una visita obligada para quien llega a Cosqu&iacute;n, Valle de Punilla, C&oacute;rdoba.<br/><br/>

                Balnearios como Cosco&iacute;no, La Toma y La Costanera, suman atractivos refrescantes a esta ciudad serrana, encantadora
                por sus paisajes y su comodidad.
                Hotel "La Posada del Padre P&iacute;o" en Cosqu&iacute;n, una opci&oacute;n para su descanso o su viaje de negocios, en la Capital Nacional
                del Folklore.<br/>

                <br/>
                <p style="text-align: center;" class="off">
                    <img src="images/img/paisaje/200/2.JPG" alt="images/img/paisaje/200/2.JPG"/>
                    <img src="images/img/paisaje/200/3.JPG" alt="images/img/paisaje/200/3.JPG"/>
                    <img src="images/img/paisaje/200/4.JPG" alt="images/img/paisaje/200/4.JPG"/>
                </p>
                <!--La ubicaci&oacute;n la podes encontrar en Google Earth. Cosquin, C&oacute;rdoba. Argentina.<br/>
                31º 14' 38.7'' S     64º 27' 34.98'' O. La propiedad est&aacute; junto al Rio Cosqu&iacute;n y tiene forma de "H"<br/>
                --><br/>
            </div>
        </div>

<?php include 'footer.php'; ?>