/*
* Dependencias
*/

var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	minifyCss = require('gulp-minify-css'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant');


/*
* Tasks
*/


gulp.task('js', function () {
	gulp.src('pre-scripts/*.js')
	.pipe(concat('all.js'))
	.pipe(uglify())
	.pipe(gulp.dest('scripts/'))
});

gulp.task('css', function () { 
    return gulp.src('pre-styles/*.css')
		.pipe(minifyCss({}))
        .pipe(concat('all.css'))
        .pipe(gulp.dest('css'));
});

gulp.task('images', function () {
    return gulp.src(['images/*.*'])
        .pipe(imagemin({
        	progressive: true,
        	svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        optimizationLevel
        .pipe(gulp.dest('images-pro/'));
});

